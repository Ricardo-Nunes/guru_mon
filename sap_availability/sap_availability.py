#!/opt/python39/bin/python3
#############################################################
##24x7 Python Plugin SAP Availability 2020-11-13 ############
##By Ricardo Nunes - ricardo.nunes at skyone.solutions ######
#############################################################
#############################################################
import pyrfc
import sys,json
import configparser 
import json 
import argparse
 
PLUGIN_VERSION = 1 ###Mandatory -If any changes in the plugin metrics, increment the version number.    
HEARTBEAT="true"  ###Mandatory -Set this to true to receive alerts when there is no data from the plugin within the poll interval
METRIC_UNITS = {'SAP_Instances': 'count'}

#configs SAP 
FM = 'TH_SERVER_LIST'
conn_cfg_path_file = '/opt/SK1_MON/conf/sapconn.cfg'

config = configparser.ConfigParser()
config.read(conn_cfg_path_file)
user = config.get('default', 'user')
passwd = config.get('default', 'passwd')
ashost = config.get('default', 'ashost')
sysnr = config.get('default', 'sysnr')
client = config.get('default', 'client')



help_desc = 'SkyOne - SAP Server Availability'

class Plugin():
    def __init__(self):
        self.data = {}
        self.data["plugin_version"]  = PLUGIN_VERSION
        self.data["heartbeat_required"]  = HEARTBEAT
        self.data["units"] = METRIC_UNITS   ###Comment this line, if you haven't defined METRIC_UNITS
        
    def mon_coll(self):
        conn = pyrfc.Connection(user=user, passwd=passwd, ashost=ashost, sysnr=sysnr, client=client)
        result=conn.call(FM)           
#Parece que a BAPI para fechar conexao nao funciona bem em algumas vesoes. Comentei a linha para teste.
#        conn.call('BAPI_XMI_LOGOFF', INTERFACE='XAL')
        conn.close()                                 
        return result
        
    def getData(self):  ### The getData method contains Sample data. User can enhance the code to fetch Original data
        try: ##set the data object based on the requirement
            FM_RETURN = plugin.mon_coll()
            servers = 0
            for key in FM_RETURN['LIST']:
                if key['NAME'] is not None:
                    servers += 1
                else:
                    servers = 1
            self.data["SAP_Instances"] = servers  
            pass
        except Exception as e:
            self.data['status']=0    ###OPTIONAL-In case of any errors,Set 'status'=0 to mark the plugin as down.
            self.data['msg'] = str(e)  ###OPTIONAL- Set the custom message to be displayed in the "Errors" field of Log Report
        return self.data
        
        
##validation method - This is only for Output validation purpose
def validatePluginData(result):
     obj,mandatory ={'Errors':""},['heartbeat_required','plugin_version']
     value={'heartbeat_required':["true","false",True,False],'status':[0,1]}
     for field in mandatory:
        if field not in result:
            obj['Errors']=obj['Errors']+"# Mandatory field "+field+" is missing #"
     for field,val in value.items():
        if field in result and result[field] not in val:
            obj['Errors']=obj['Errors']+"# "+field+" can only be "+str(val)
     if 'plugin_version' in result and not isinstance(result['plugin_version'],int):
        obj['Errors']=obj['Errors']+"# Mandatory field plugin_version should be an integer #"
     RESERVED_KEYS='plugin_version|heartbeat_required|status|units|msg|onchange|display_name|AllAttributeChart'
     attributes_List=[]
     for key,value in result.items():
         if key not in RESERVED_KEYS:
            attributes_List.append(key)
     if len(attributes_List) == 0:
        obj['Errors']="# There should be atleast one \"Number\" type data metric present #"
     if obj['Errors'] !="":
        obj['Result']="**************Plugin output is not valid************"
     else:
        obj['Result']="**************Plugin output is valid**************"
        del obj['Errors']
     result['validation output']= obj
      
if __name__ == '__main__':
    plugin = Plugin()
    data = plugin.getData()
    parser = argparse.ArgumentParser()
    parser.add_argument("param", nargs='?', default="dummy")
    args = parser.parse_args()
    if args.param != 'skipvalidation':
        validatePluginData(data)
    print(json.dumps(data, indent=4, sort_keys=True))  ###Print the output in JSON format
